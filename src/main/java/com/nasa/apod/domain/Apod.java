package com.nasa.apod.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.data.rest.core.annotation.RestResource;

import lombok.Data;

@Entity
@Data
@RestResource(rel = "apods", path = "apod")
public class Apod {

	@Id
	@GeneratedValue
	long id;
	
	@Column(nullable = false, unique = true)
	String date;
	
	@Column(length=3000)
	String explanation;
	
	@Column
	String hdurl;
	
	@Column
	String media_type;
	
	@Column
	String service_version;
	
	@Column
	String title;
	
	@Column
	String url;


}
