package com.nasa.apod.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.nasa.apod.domain.Apod;


@RepositoryRestResource(path = "apods")
public interface ApodRepository  extends JpaRepository<Apod, Long>  {
	
	public Apod findByDate(String Date);
	
	public List<Apod> deleteByDate(String date);
	public void deleteById(Long id);

}
