package com.nasa.apod.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.nasa.apod.domain.Apod;
import com.nasa.apod.repository.ApodRepository;

@Service
public class ApodService {
	@Autowired
	ApodRepository repository;

	public Apod loadData(String apiKey, String date, Boolean hd) {

		String url;
		url = String.format("https://api.nasa.gov/planetary/apod?api_key=%s&date=%s&hd=%b", apiKey, date, hd);
		ResponseEntity<Apod> responseEntity = new RestTemplate().getForEntity(url, Apod.class);

		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			Apod response = new Apod();
			response = responseEntity.getBody();
			repository.save(response);
			return response;
		} else {
			return null;
		}

	}

	public List<Apod> loadRangeData(String apiKey, String startDate, String endDate, Boolean hd) {
		String url;

		List<LocalDate> i = getDateList(startDate, endDate);
		List<Apod> responseList = new ArrayList<>();

		for (LocalDate localDate : i) {

			url = String.format("https://api.nasa.gov/planetary/apod?api_key=%s&date=%s&hd=%b", apiKey, localDate, hd);
			ResponseEntity<Apod> responseEntity = new RestTemplate().getForEntity(url, Apod.class);

			Apod response = new Apod();
			response = responseEntity.getBody();
			repository.save(response);
			responseList.add(response);

		}
		return responseList;

	}

	public Apod updateApodById(Long id, Apod newApod) {

		repository.findById(id).map(apod -> {
			return repository.save(newApod);
		});
		return newApod;
	}

	public Apod updateApodByDate(String date, Apod newApod) {

		Apod dateToId = repository.findByDate(date);

		repository.findById(dateToId.getId()).map(apod -> {

			return repository.save(newApod);
		});
		return newApod;
	}
	@Transactional
	public String deleteRangeData(String startDate, String endDate) {

		List<LocalDate> i = getDateList(startDate, endDate);

		for (LocalDate localDate : i) {

			String date = localDate.toString();

			repository.deleteByDate(date);
		}
		return "Data dated between: " + startDate + " - " + endDate + " has been successfully deleted!";
	}
	@Transactional
	public String deleteByDate(String date) {
		
		repository.deleteByDate(date); //Cambiar a servicio
		
		return "Data with Date: " +date + " has been successfully deleted!";
	}
	@Transactional
	public String deleteById(Long id) {
		
		repository.deleteById(id); //Cambiar a servicio

		return "Data with Id: " +id + " has been successfully deleted!";
	}
	
//	public Page<List<ApodDTO>> findAll(){
//		
//		return Page<repository.findAll())>;
//	}
	

	private static List<LocalDate> getDateList(String startDate, String endDate) {

		final LocalDate start = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		final LocalDate end = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

		final int days = (int) start.until(end, ChronoUnit.DAYS);

		return Stream.iterate(start, d -> d.plusDays(1)).limit(days + 1).collect(Collectors.toList());
	}

}
