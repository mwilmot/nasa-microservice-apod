package com.nasa.apod.config;
//package com.nasa.config;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.factory.PasswordEncoderFactories;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled=true, prePostEnabled=true)
//public class ConfigSecurity extends WebSecurityConfigurerAdapter {
//	@Autowired
//    DataSource dataSource;
//
//	//Enable jdbc authentication
//    @Autowired
//    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication().dataSource(dataSource);
//    }
//
//	@Override
//	public void configure(WebSecurity web) throws Exception {
//		web.ignoring().antMatchers("/resources/**");
//	}
//
////	@Override
////	protected void configure(HttpSecurity http) throws Exception {
//////		http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/welcome").hasAnyRole("USER", "ADMIN")
//////				.antMatchers("/apod/all").hasAnyRole("USER", "ADMIN").antMatchers("/apod/import/date")
//////				.hasAnyRole("ADMIN").anyRequest().authenticated().and().formLogin().permitAll()
//////				.and().logout().permitAll();
//////
//////		http.csrf().disable();
////		
////		http.
////		authorizeRequests()
////		.antMatchers("/**").hasRole("ADMIN")
////		.antMatchers(HttpMethod.GET, "login/").permitAll().anyRequest().authenticated()
////		.and()
////        .csrf().disable()
////        .formLogin().disable();		
////		//http.authorizeRequests().antMatchers("/apod/findAll").hasRole("USER").and().formLogin();
////		//http.authorizeRequests().antMatchers(HttpMethod.GET, "login/").permitAll().anyRequest().authenticated();	
////		super.configure(http);
////	}
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//
//		//http.authorizeRequests().antMatchers("/**").hasRole("BASIC").and().formLogin();
//		http.authorizeRequests().antMatchers(HttpMethod.GET, "login/").permitAll()
//		.antMatchers(HttpMethod.GET, "h2/").permitAll().anyRequest().authenticated();	
//		super.configure(http);
//	}
//
//	@Autowired
//	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//		
//		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
//		auth
//			.inMemoryAuthentication()
//				.withUser("Attie").password(encoder.encode("popo123")).roles("BASIC");
//		
//		auth
//		.inMemoryAuthentication()
//		.withUser("Carrax").password(encoder.encode("popo123")).roles("BASIC");
//		
//		auth
//		.inMemoryAuthentication()
//		.withUser("GHB").password(encoder.encode("popo123")).roles("ADMIN");
//	}
//}
