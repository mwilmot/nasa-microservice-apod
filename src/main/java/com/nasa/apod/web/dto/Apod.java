package com.nasa.apod.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "DTO to APOD")
public class Apod {
	
	@JsonIgnore
	long id;
	
	String date;
	
	String explanation;
	
	String hdurl;
	
	String media_type;
	
	String service_version;
	
	String title;
	
	String url;
}
