package com.nasa.apod.web.controller;

import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.nasa.apod.domain.Apod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="Apod Microservice", description="APOD API REST - NASA MICROSERVICE")
public interface ApodController {

	@ApiOperation(value="Create a new Apod")
	ResponseEntity<Apod> newApod(Apod newApod);
	
	@ApiOperation(value="Search all records from local database")
	ResponseEntity<Iterable<Apod>> findAllApods(); //int page, int pageSize

	@ApiOperation(value="Search record by Id from local database")
	ResponseEntity<Optional<Apod>> findById(Long id);
	
	@ApiOperation(value="Update Apod by Id")
	ResponseEntity<Apod> updateApodById(Apod newApod, Long id);
	
	@ApiOperation(value="Delete a record by Id")
	ResponseEntity<String> deleteById(Long id);

//	@ApiOperation(value="Import data from API NASA - APOD")
//	ResponseEntity<ApodDTO> loadData(String apiKey, String date, Boolean hd);
//
//	@ApiOperation(value="Import data range from API NASA - APOD")
//	ResponseEntity<List<ApodDTO>> loadRangeData(String apiKey, String startDate, String endDate, Boolean hd);
//
//
//
//	@ApiOperation(value="Update Apod by Date")
//	ResponseEntity<ApodDTO> updateApodByDate(ApodDTO newApod, String date);
//	
//
//	@ApiOperation(value="Search record by Date from local database")
//	ResponseEntity<ApodDTO> findByDate(String date);
//
//	
//	@ApiOperation(value="Delete range of records by Date")
//	ResponseEntity<String> deleteRangeData(String startDate, String endDate);
//	
//	@ApiOperation(value="Delete a record by Date")
//	ResponseEntity<String> deleteByDate(String date);


}