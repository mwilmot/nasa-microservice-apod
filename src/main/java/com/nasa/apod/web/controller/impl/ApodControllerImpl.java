package com.nasa.apod.web.controller.impl;

import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nasa.apod.domain.Apod;
import com.nasa.apod.repository.ApodRepository;
import com.nasa.apod.service.ApodService;
import com.nasa.apod.web.controller.ApodController;

@RestController
@RequestMapping("/imageByDay")
public class ApodControllerImpl implements ApodController {

	@Autowired
	ApodService service;
	@Autowired
	ApodRepository repository;
	
	/*CREATE new records in DATABASE*/
	@Override
	@PostMapping
	public ResponseEntity<Apod> newApod(@RequestBody Apod newApod) {
		return ResponseEntity.ok(repository.save(newApod));
	}
	/*UPDATE records in DATABASE*/
	
	@Override
	@PutMapping("/{id}")
	public ResponseEntity<Apod> updateApodById(@RequestBody Apod newApod, @PathParam("id") Long id) {
		
		return ResponseEntity.ok(service.updateApodById(id, newApod));
	}
		@Override
		@GetMapping("/{id}")
		public ResponseEntity<Optional<Apod>> findById(@PathParam("id") Long id) {
			
			return ResponseEntity.ok(repository.findById(id));
		}
		/* FIND data from DATABASE*/
		
		@Override
		@DeleteMapping("/{id}")
		public ResponseEntity<String> deleteById(@PathParam("id") Long id) {
	
			return ResponseEntity.ok(service.deleteById(id)); //Cambiar a servicio
		}
	@Override
	@GetMapping
	public ResponseEntity<Iterable<Apod>> findAllApods() {
		return ResponseEntity.ok(repository.findAll());

	}

	
//	/* IMPORT data from NASA API*/
//	
//	@Override
//	@GetMapping("/import/date{apiKey}{date}{hd}")
//	public ResponseEntity<ApodDTO> loadData(@PathParam("apiKey") String apiKey, @PathParam("date") String date, @PathParam("hd") Boolean hd) {
//
//		
//		return ResponseEntity.ok(service.loadData(apiKey, date, hd));
//
//	}
//	
//	@Override
//	@GetMapping("/import/rangeDate{apiKey}{startDate}{endDate}{hd}")
//	public ResponseEntity<List<ApodDTO>> loadRangeData(@PathParam("apiKey") String apiKey, @PathParam("startDate") String startDate,
//			@PathParam("endDate") String endDate, @PathParam("hd") Boolean hd) {
//
//		return ResponseEntity.ok(service.loadRangeData(apiKey, startDate, endDate, hd));
//
//	}
//	
//	
//	

//	@Override
//	@PutMapping("/updateByDate{date}")
//	public ResponseEntity<ApodDTO> updateApodByDate(@RequestBody ApodDTO newApod, @PathParam("date") String date) {
//		
//		return ResponseEntity.ok(service.updateApodByDate(date, newApod));
//		
//	}
//	

//	
//	@Override
//	@GetMapping("/apods/{date}")
//	public ResponseEntity<ApodDTO> findByDate(@PathParam("date") String date) {
//
//		return ResponseEntity.ok(repository.findByDate(date));
//	}

//	
//	/* DELETE data from DATABASE*/
//
//	@Override
//	@DeleteMapping("/deleteRangeDate{startDate}{endDate}")
//	public ResponseEntity<String> deleteRangeData(@PathParam("startDate") String startDate,
//			@PathParam("endDate") String endDate) {
//		
//		return ResponseEntity.ok(service.deleteRangeData(startDate, endDate));
//		
//	}
//	@Override
//	@DeleteMapping("/deleteByDate{date}")
//	public ResponseEntity<String> deleteByDate(@PathParam("date") String date) {
//
//		return ResponseEntity.ok(service.deleteByDate(date));
//	}

//	
//	

}
